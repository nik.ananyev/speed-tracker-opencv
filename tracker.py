import math
import numpy as np

class EuclideanDistTracker:
    def __init__(self):
        # Store the center positions of the objects
        self.center_points = {}
        # Keep the count of the IDs
        # each time a new object id detected, the count will increase by one
        self.id_count = 0
        self.frame_count = 0
        self.frame_rate = 25
        self.metrs_in_pixel = 0.012
        self.detected = {}
    
    def update(self, objects_rect):
        # Objects boxes and ids
        objects_bbs_ids = []
        self.frame_count +=1
        adeed_center_points = []
        # Get center point of new object
        for rect in objects_rect:
            x, y, w, h = rect
            cx = (x + x + w) // 2
            cy = (y + y + h) // 2
            dx = cx
            dy = cy
            # Find out if that object was detected already
            same_object_detected = False
            for id, pt in self.center_points.items():
                dist = math.hypot(cx - pt[0], cy - pt[1])

                if dist < 40:
                    frame = self.center_points[id][2]
                    dx = self.center_points[id][3]
                    dy = self.center_points[id][4]
                    self.center_points[id] = (cx, cy, frame, dx, dy)
                    objects_bbs_ids.append([x, y, w, h, id])
                    same_object_detected = True
                    break

            # New object is detected we assign the ID to that object
            if same_object_detected is False:
                self.center_points[self.id_count] = (cx, cy, self.frame_count, dx, dy)
                objects_bbs_ids.append([x, y, w, h, self.id_count])
                adeed_center_points.append([x, y, w, h, self.id_count])
                self.id_count += 1

        # Clean the dictionary by center points to remove IDS not used anymore
        new_center_points = {}
        for obj_bb_id in objects_bbs_ids:
            _, _, _, _, object_id = obj_bb_id
            center = self.center_points[object_id]
            new_center_points[object_id] = center
        
        remain_center_points = {}
        for obj_bb_id in adeed_center_points:
            _, _, _, _, object_id = obj_bb_id
            center = self.center_points[object_id]
            remain_center_points[object_id] = center
        adeed_center_points = remain_center_points.copy()

        remain_center_points = new_center_points.copy()
        for point in new_center_points.copy():
            if point in adeed_center_points:
                remain_center_points.pop(point)

        removed_centre_points_ids =  set(self.center_points).symmetric_difference(set(remain_center_points)).symmetric_difference(set(adeed_center_points))
        removed_centre_points = {}
        for id in removed_centre_points_ids:
            removed_centre_points[id] = self.center_points.get(id)
        
        speeds = self.getSpeed(list(removed_centre_points.items()))
        # Update dictionary with IDs not used removed
        self.center_points = new_center_points.copy()

        #if self.center_points != self.old_center_points:
            #print(set(self.center_points).symmetric_difference(set(self.old_center_points)))
        return objects_bbs_ids, speeds, new_center_points


    def getSpeed(self, centre_points):
        speeds =[]
        for centre_point in centre_points:
            detec_time = centre_point[1][2]
            time_in_frames = self.frame_count - detec_time;
            if time_in_frames > self.frame_rate:
                time_in_seconds = time_in_frames / self.frame_rate
                dist = self.getDistance(centre_point[1][0], centre_point[1][1], centre_point[1][3], centre_point[1][4]) * self.metrs_in_pixel
                speed = round(dist/time_in_seconds, 4)
                speeds.append(speed)
        return speeds

    def getDistance(self, cx, cy, dx, dy):
        return round(math.sqrt((cx-dx)**2 + (cy-dy)**2), 4)