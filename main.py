import cv2
from tracker import *

cap = cv2.VideoCapture("Record1.mp4")
tracker = EuclideanDistTracker()
detecSpeeds = []
cap.set(3,1280)
cap.set(4,700)
ret, frame1 = cap.read()
ret, frame2 = cap.read()
#h, w, _ = frame1.shape #размер видео
#print(h, w)  
oldContours = []
while cap.isOpened(): # метод isOpened() выводит статус видеопотока

    alldiff = cv2.absdiff(frame1, frame2) # нахождение разницы двух кадров, которая проявляется лишь при изменении одного из них, т.е. с этого момента наша программа реагирует на любое движение.
    diff = alldiff[200: 576, 300: 928] #область для отслеживания  
    area1 = frame1[200: 576, 300: 928]

    gray = cv2.cvtColor(diff, cv2.COLOR_BGR2GRAY) # перевод кадров в черно-белую градацию
    
    blur = cv2.GaussianBlur(gray, (5, 5), 0) # фильтрация лишних контуров

    _, thresh = cv2.threshold(blur, 20, 255, cv2.THRESH_BINARY) # метод для выделения кромки объекта белым цветом
 
    dilated = cv2.dilate(thresh, None, iterations = 1) # данный метод противоположен методу erosion(), т.е. эрозии объекта, и расширяет выделенную на предыдущем этапе область
 
    contours, _ = cv2.findContours(dilated, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE) # нахождение массива контурных точек
    
    detections = []
    if len(contours)>0: #проверка найдены ли контуры
        oldContours=contours
    else:
        contours = oldContours
    for contour in contours:
        (x, y, w, h) = cv2.boundingRect(contour) # преобразование массива из предыдущего этапа в кортеж из четырех координат
        if cv2.contourArea(contour)<1000: # условие по площади объекта
            continue
        detections.append([x, y, w, h])

    if(len(detections)>0):
        boxes_ids, speeds, newpoints = tracker.update(detections)
        for box_id in boxes_ids:
            x, y, w, h, id = box_id
            cv2.putText(area1, str(id), (x, y+h-10), cv2.FONT_HERSHEY_PLAIN, 2, (255, 0, 0), 2)
            cv2.rectangle(area1, (x, y), (x + w, y + h), (0, 255, 0), 3)
        if len(speeds)>0:
            for speed in speeds:
                detecSpeeds.append(speed)
                print(speed)

    cv2.imshow("frame1", frame2)
    cv2.imshow("diff", diff)
    cv2.imshow("thresh", dilated)
    cv2.imshow("frame2", frame1)
    frame1 = frame2  #
    ret, frame2 = cap.read() # 

    if cv2.waitKey(40) == 27:
        break
    
if len(detecSpeeds)>0:
    print('Average speed = '+ str(round(sum(detecSpeeds)/len(detecSpeeds), 4)))
    print('Max speed = '+ str(max(detecSpeeds)))
cap.release()
cv2.destroyAllWindows()




